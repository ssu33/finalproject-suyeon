
package com.example.final_project.controller;

import com.example.final_project.domain.Response;
import com.example.final_project.domain.dto.post.*;
import com.example.final_project.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.Authentication;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/posts")
@Slf4j
public class PostController {

    private final PostService postService;

    // 등록
    @PostMapping
    public Response<PostCreateResponse> create(@RequestBody PostCreateRequest postCreateRequest,  @ApiIgnore Authentication authentication) {
        PostDto postDto = postService.create(postCreateRequest, authentication.getName());
        PostCreateResponse response = new PostCreateResponse("작성 완료",postDto.getId());
        return Response.success(response);
    }

    /*
    // 수정
    @PutMapping("/{postId}")
    public Response<PostEditResponse> edit(@PathVariable(name = "postId") Long postId,
                                           @RequestBody PostCreateRequest postEditRequest,
                                           Authentication authentication){
        PostEditResponse postEditResponse = new PostEditResponse(postId);
        return Response.success(postEditResponse);
    }

     */

    /*
    //삭제
    @DeleteMapping("/{postId}")
    public Response<PostDeleteResponse> delete(@PathVariable(name = "postId") Long postId, Authentication authentication){
        postService.delete(postId,authentication.getName());
        PostDeleteResponse postDeleteResponse = new PostDeleteResponse(postId);
        return Response.success(postDeleteResponse);
    }
     */
}


