
package com.example.final_project.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@RequestMapping("/api/v1")
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "happy_new_year";
    }
}
