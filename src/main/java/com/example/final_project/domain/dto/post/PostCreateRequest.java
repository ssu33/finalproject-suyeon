package com.example.final_project.domain.dto.post;

import com.example.final_project.domain.Post;
import com.example.final_project.domain.User;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class PostCreateRequest {
    private String title;
    private String body;

}
