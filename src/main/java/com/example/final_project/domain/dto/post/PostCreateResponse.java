package com.example.final_project.domain.dto.post;

import com.example.final_project.domain.Post;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class PostCreateResponse {
    private String message;
    private Long postId;

    public PostCreateResponse(Post post) {
        this.message = "등록 완료!";
        this.postId = post.getId();
    }
}
