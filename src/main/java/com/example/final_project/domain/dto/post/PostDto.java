package com.example.final_project.domain.dto.post;

import com.example.final_project.domain.User;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostDto {
    private Long id;
    private String userName;
    private String title;
    private String body;
    private LocalDate registeredAt;
}
