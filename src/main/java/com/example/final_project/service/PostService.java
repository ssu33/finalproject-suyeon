
package com.example.final_project.service;

import com.example.final_project.domain.Post;
import com.example.final_project.domain.User;
import com.example.final_project.domain.dto.post.*;
import com.example.final_project.exception.AppException;
import com.example.final_project.exception.ErrorCode;
import com.example.final_project.repository.PostRepository;
import com.example.final_project.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostDto create(PostCreateRequest request, String userName) {

        User user = userRepository.findByUserName(userName)
                .orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND,userName+"없음"));
        Post post = new Post(request.getTitle(), request.getBody(),user);

        postRepository.save(post);
        return PostDto.builder()
                .id(post.getId())
                .body(post.getBody())
                .title(post.getTitle())
                .build();

    }

    /*
    public PostEditResponse edit(PostEditRequest request, String userName, Long id) {
        User user = userRepository.findByUserName(userName)
                .orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND,userName+"가 없음"));
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, id + "가 없음"));
        return new PostEditResponse(postRepository.save(post));
    }

     */
}
