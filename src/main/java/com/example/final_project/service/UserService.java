package com.example.final_project.service;

import com.example.final_project.domain.User;
import com.example.final_project.exception.AppException;
import com.example.final_project.exception.ErrorCode;
import com.example.final_project.repository.UserRepository;
import com.example.final_project.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    @Value("${jwt.token.secret}")
    private String secretKey;
    private Long expiredMs = 1000*60*60l;

    public String join(String userName, String password){

        // USERNAME 중복체크하기
        userRepository.findByUserName(userName)
                .ifPresent(user -> {
                        throw new AppException(ErrorCode.USERNAME_DUPLICATED, userName + "는 이미 있습니다");
    });

        // 저장하기
        User user = User.builder()
                .userName(userName)
                .password(encoder.encode(password))
                .build();
        userRepository.save(user);

        return "SUCCESS";
    }


    public String login(String userName, String password) {

        // userName 없음
        User selectedUser = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, userName + "이 없음"));

        // password 틀림
        if(!encoder.matches(password, selectedUser.getPassword())){
            throw new AppException(ErrorCode.INVALID_PASSWORD, "password를 잘못 입력했습니다.");
        }

        String token = JwtTokenUtil.createToken(selectedUser.getUserName(), secretKey, expiredMs);

        // 앞에서 Exception안났으면 토큰 발행
        return JwtTokenUtil.createToken(userName, secretKey, expiredMs);
    }
}
